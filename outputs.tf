output "ec2_public_dns" {
  value = aws_instance.main.public_dns
}

output "ec2_public_ip" {
  value = aws_instance.main.public_ip
}

output "ec2_primary_ssh_string" {
  description = "Copy paste this string to SSH into the primary."
  value       = "ssh -i ${var.myaws_keypair}.pem ubuntu@${aws_instance.main.public_ip}"
}
